// Paolo the Crazy Professor!!!
// Sander here, I'm an explorer of skills
// Midhun Explorer
// Johan Festival Enthusiast
// Hakan Musician & Kickboxer
const { wordMultiplier, mostExpensive, shouldInviteMoreWomen } = require('./index');

describe('wordMultiplier', () => {
    it('checks if first parameter is a string', () => {
        expect(wordMultiplier("This is a string", 3, 5)).not.toBeNull();
        expect(wordMultiplier("", 3, 5)).toBeNull();
        expect(wordMultiplier(true, 3, 5)).toBeNull();
    });

    it('checks if second parameter is within range', () => {
        expect(wordMultiplier("This is a string", 7, 5)).toBeNull();
        expect(wordMultiplier("This is a string", -3, 5)).toBeNull();
        expect(wordMultiplier("This is a string", 4, 5)).toBeNull();
    });

    it('checks if third parameter is a valid multiplier', () => {
        expect(wordMultiplier("This is a string", 3, 0)).toBeNull();
        expect(wordMultiplier("This is a string", 3, -5)).toBeNull();
    });

    it('correctly finds the corresponding word in the given string', () => {
        expect(wordMultiplier("This is a string", 3, 5)).toContain("string");
        expect(wordMultiplier("This is a string", 0, 5)).toContain("This");
        expect(wordMultiplier("This is a string", 2, 5)).toContain("a");
    });
});

describe("mostExpensive", () => {
    it('should return the name of the most expensive jewelry piece', () => {
        expect(
            mostExpensive({
                "Diamond Earrings": 980,
                "Gold Watch": 250,
                "Pearl Necklace": 4650
            })
        ).toEqual("The most expensive one is the Pearl Necklace");

        expect(
            mostExpensive({
                "Silver Bracelet": 280,
                "Gemstone Earrings": 180,
                "Diamond Ring": 3500
            })
        ).toEqual("The most expensive one is the Diamond Ring");
    });
});

describe("shouldInviteMoreWomen", () => {
    let attendees = []

    it('should return false if majority of attendees is woman', () => {
        attendees = [-1, -1, -1, 1, 1];
        expect(shouldInviteMoreWomen(attendees)).toBe(false);
    })

    it('should return false if half of attendees is woman', () => {
        attendees = [-1, -1, 1, 1];
        expect(shouldInviteMoreWomen(attendees)).toBe(false);
    })

    it('should return false if all attendees are women', () => {
        attendees = [-1, -1, -1, -1];
        expect(shouldInviteMoreWomen(attendees)).toBe(false);
    })

    it('should return true if minority of attendees is woman', () => {
        attendees = [-1, -1, 1, 1, 1];
        expect(shouldInviteMoreWomen(attendees)).toBe(true);
    })

    it('should return true if none of attendees is woman', () => {
        attendees = [1, 1, 1, 1, 1];
        expect(shouldInviteMoreWomen(attendees)).toBe(true);
    })

    it('should return false if noone is attending', () => {
        attendees = [];
        expect(shouldInviteMoreWomen(attendees)).toBe(false);
    })
});
