const wordMultiplier = (sentence, wordIndex, multiplier) => {
    const isInvalidSentence = !(typeof sentence === "string" && sentence.length);
    const isInvalidMultiplier = multiplier <= 0;
    if (isInvalidSentence || isInvalidMultiplier) return null;

    const words = sentence.split(" ");
    if (wordIndex >= words.length || wordIndex < 0) return null;
    const keyWord = words[wordIndex];

    return Array(multiplier).fill(keyWord).join("-");
}

const mostExpensive = obj => {
    const sortedByPriceDesc = Object.entries(obj)
        .sort(([, priceA], [, priceB]) => priceB - priceA)
    const [[jewelryName]] = sortedByPriceDesc;

    return `The most expensive one is the ${jewelryName}`
};

const shouldInviteMoreWomen = listOfAttendees => {
    const amountOfMen = listOfAttendees.filter(attendee => attendee === 1).length;
    const amountOfWomen = listOfAttendees.filter(attendee => attendee === -1).length;

    return amountOfWomen < amountOfMen;
}

module.exports = {
    wordMultiplier,
    mostExpensive,
    shouldInviteMoreWomen
}
